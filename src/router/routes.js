import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";
import Login from "@/pages/Login.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import Advcampaigns from "@/pages/Advcampaigns";
import Categories from "@/pages/Categories.vue";
import AdvcampaignCategories from "@/pages/AdvcampaignCategories";

const routes = [
    {path: "/login", name: "Логин", component: Login, meta: {requiresVisitor: true}},
    {
        path: "/",
        component: DashboardLayout,
        redirect: "/dashboard",
        children: [
            {path: "/dashboard", name: "Главная", component: Dashboard},
            {path: "/advcampaigns", name: "Кампании", component: Advcampaigns},
            {path: "/categories", name: "Категории", component: Categories},
            {path: "/advcampaign_categories", name: "Категории кампаний", component: AdvcampaignCategories},
        ],
        meta: {requiresAuth: true}
    },
    {path: "*", component: NotFound}
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
