import moment from 'moment';

const defaultDates = {
    srvDateFormat: 'YYYY-MM-DD',
    srvTimeFormat: 'HH:mm:ss',
    get srvDateTimeFormat() {
        return this.srvDateFormat + ' ' + this.srvTimeFormat;
    },

    cliDateFormat: 'ddd DD/MMM/YYYY'.toLowerCase(),
    cliTimeFormat: 'hh:mm a',
    get cliDateTimeFormat() {
        return this.cliDateFormat + ' ' + this.cliTimeFormat;
    },
};
const keyStr = "ABCDEFGHIJKLMNOP" +
               "QRSTUVWXYZabcdef" +
               "ghijklmnopqrstuv" +
               "wxyz0123456789+/" +
               "=";

const messageTypes = [
    'success',
    'warning',
    'info',
    'error'
];

let filters = {
    formatDate: function (text_date, only_date) {
        return methods.formatDate(text_date, false, only_date);
    },

    formatDateToDayDate: function (date) {
        return methods.formatDateToDayDate(date);
    },

    shortDateTime: function (text_date) {
        return methods.convertDate(text_date, defaultDates.srvDateTimeFormat, 'DD/MM/YYYY hh:mm a');
    },

    capitalize: function (value) {
        if (!value) return '';
        value = value.toString();
        return value.toLowerCase()
            .split(' ')
            .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
            .join(' ');
    },

    truncate: function (text, stop, clamp) {
        return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
    },

};

let methods = {
    getQueryStringByName: function (name) {
        let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        let r = window.location.search.substr(1).match(reg);
        let context = "";
        if (r !== null)
            context = r[2];
        reg = null;
        r = null;
        return context === null || context === "" || context === "undefined" ? "" : decodeURIComponent(context);
    },

    convertDate: function (data, fromFormat, toFormat) {
        return data ? moment(data, fromFormat).format(toFormat) : '';
    },

    getFormat: function(data) {
        return moment(data).creationData().format
    },

    formatDate: function (data, forDatabase, onlyDate) {
        let currentFormat = forDatabase ? defaultDates.cliDateTimeFormat : (onlyDate ? defaultDates.srvDateFormat : defaultDates.srvDateTimeFormat);
        let toFormat = forDatabase ? defaultDates.srvDateTimeFormat : (onlyDate ? defaultDates.cliDateFormat : defaultDates.cliDateTimeFormat);

        return methods.convertDate(data, currentFormat, toFormat);
    },
    convertDateToMoment: function (data, forDatabase, onlyDate) {
        let currentFormat = forDatabase ? defaultDates.cliDateTimeFormat : (onlyDate ? defaultDates.srvDateFormat : defaultDates.srvDateTimeFormat);
        return moment(data, currentFormat);
    },

    formatDateToHumanDate: function (date) {
        return this.convertDate(date,'YYYY-MM-DD HH:mm:ss', 'MMM DD');
    },

    formatDateToDayDate: function (date, format, list = [], global) {
        if(format === list[0]){
            return this.convertDate(date, 'YYYY-MM-DD', global);
        }else {
            return this.convertDate(date,'YYYY-MM-DD HH:mm:ss', global+' hh:mm a');
        }
    },
    formatTime: function (time, format) {
        let parsedTime = time.split(':');
        let date = new Date(1970,1,1, parsedTime[0], parsedTime[1]);
        return moment(date).format(format);
    },
    addClassIfScroll: function (element, className) {
        if (element) {
            let hasScroll = element.scrollHeight > element.clientHeight;
            if (hasScroll) {
                element.className += " " + className;
            } else {
                element.classList.remove(className);
            }
        }
    },

    downloadFile(file, title) {
        const url = window.URL.createObjectURL(new Blob([file]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', title);
        document.body.appendChild(link);
        link.click();
    },

    formatPhoneNumber: function (value) {
        const mask = [];
        const maxLength = 15; //for digits only
        let newValue = value.replace(/[^0-9+]/g, '').slice(0, maxLength);
        if (newValue !== '') {
            // if(newValue[0] && newValue[0] == '0'){
            //     newValue = '+61' + newValue.slice(1);
            // }
            _.forEach(mask, (char, index) => {
                if (newValue[index] && newValue[index] !== char) {
                    newValue = newValue.slice(0, index) + char + newValue.slice(index);
                }
            });
        }
        return newValue;
    },

    formatTitleWithoutSpecialSymbols: function (value) {
        const mask = [];
        const maxLength = 255;
        let newValue = value.replace(/[`~!@#№$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '').slice(0, maxLength);

        if (newValue !== '') {

            _.forEach(mask, (char, index) => {
                if (newValue[index] && newValue[index] !== char) {
                    newValue = newValue.slice(0, index) + char + newValue.slice(index);
                }
            });
        }
        return newValue;
    },

    createTextLinks: function (text) {
        return (text || "").replace(
            /([^\S]|^)(((https?\:\/\/)|(www\.))(\S+))/gi,
            function (match, space, url) {
                let hyperlink = url;
                if (!hyperlink.match('^https?:\/\/')) {
                    hyperlink = 'http://' + hyperlink;
                }
                return space + '<a href="' + hyperlink + '" target="_blank">' + url + '</a>';
            }
        );
    },

    isMobile: function () {
        let check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    },

    isTablet: function () {
        const userAgent = navigator.userAgent.toLowerCase();
        return /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(userAgent);
    },

    encode64: function (input) {
        input = escape(input);
        let output = "";
        let chr1, chr2, chr3 = "";
        let enc1, enc2, enc3, enc4 = "";
        let i = 0;

        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                keyStr.charAt(enc1) +
                keyStr.charAt(enc2) +
                keyStr.charAt(enc3) +
                keyStr.charAt(enc4);
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);

        return output;
    },

    decode64: function (input) {
        let output = "";
        let chr1, chr2, chr3 = "";
        let enc1, enc2, enc3, enc4 = "";
        let i = 0;

        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        let base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            // alert("There were invalid base64 characters in the input text.\n" +
            //     "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
            //     "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        do {
            enc1 = keyStr.indexOf(input.charAt(i++));
            enc2 = keyStr.indexOf(input.charAt(i++));
            enc3 = keyStr.indexOf(input.charAt(i++));
            enc4 = keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";

        } while (i < input.length);

        return unescape(output);
    },

    pluck(array, key) {
        return array.map(o => o[key]);
    },

    objectToQueryString(obj) {
        return Object.entries(obj).map(([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`).join('&');
    },
    mergeExistingFields(source, fields) {
        return _.assign(source, _.pick(fields, _.keys(source)));
    },
    cancelRequest(entity = null) {
        let entities = [];
        if (entity) {
            entities.push(entity);
        } else {
            if (this.sources) {
                Object.keys(this.sources).forEach((currentEntity) => {
                    entities.push(currentEntity);
                });
            }
        }
        entities.forEach((currentEntity) => {
            if (this.sources && this.sources[currentEntity]) {
                this.sources[currentEntity].cancel('Fetch another ' + currentEntity + ' list');
            }
        });
    },
    resetForm(fields = [], except = []) {
        if (_.isEmpty(fields)) {
            fields = _.keys(this.originForm);
            except.forEach(formKey => {
                let index = fields.indexOf(formKey);
                fields.splice(index, 1);
            });
        }
        fields.forEach((field) => {
            this.form[field] = _.cloneDeep(this.originForm[field]);
            this.cancelRequest(field);
            this.errors.clear(field);
        });
    },
    delay(ms) {
        return new Promise(r => setTimeout(r, ms));
    },
};

export {filters, methods};

export default methods;
