import axios from 'axios';

export const advcampaignsToggleActive   = params => axios.post   (server + "advcampaigns/toggle-active", params);
export const getAdvcampaignsList        = params => axios.get    (server + "advcampaigns/list", params);
