window._ = require('lodash');
window.axios = require("axios");
window.server = "https://api.olja.kz/api/";

window.axios.defaults.withCredentials = true;

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': 'Bearer ' + window.localStorage.getItem('access_token')
};

import Vue from "vue";
import App from "./App";
import ElementUI from "element-ui";
import locale from "element-ui/lib/locale/lang/ru-RU";
import "element-ui/lib/theme-chalk/index.css";
import {store} from './store/store';
import router from "./router/index";

import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";

Vue.use(ElementUI, { locale });
Vue.use(PaperDashboard);

/* eslint-disable no-new */
new Vue({
    store,
    router,
    render: h => h(App)
}).$mount("#app");
