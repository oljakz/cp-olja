import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        auth: localStorage.getItem('user_id') || null,
        authCheck: false,
    },
    getters: {
        loggedIn(state) {
            return state.token !== null && state.token !== 'undefined' && state.auth !== null && state.authCheck;
        },
        auth(state) {
            return state.auth;
        },
        token(state) {
            return state.token;
        },
        authCheck(state) {
            return state.authCheck;
        }
    },
    mutations: {
        retrieveToken(state, token) {
            state.token = token
        },
        retrieveAuth(state, payload) {
            state.auth = payload
        },
        retrieveAuthCheck(state, payload) {
            state.authCheck = payload
        },
        destroyToken(state) {
            state.token = null
        },
        destroyAuth(state) {
            state.auth = null
        },
        destroyAuthCheck(state) {
            state.authCheck = false
        },
    },
    actions: {
        checkToken(context) {
            return new Promise((resolve, reject) => {
                axios.defaults.headers.common['Authorization'] = 'Bearer ' +  this.getters.token;
                axios.post(server + 'check-token')
                    .then(response => {
                        context.commit('retrieveAuthCheck', true);
                        resolve(response);
                    })
                    .catch(error => {
                        console.log(error);
                        reject(error);
                    })
            })
        },
        retrieveToken(context, credentials) {
            return new Promise((resolve, reject) => {
                axios.post(server + 'login', {
                    email: credentials.email,
                    password: credentials.password,
                })
                    .then(response => {
                        const auth = response.data.user;
                        const token = response.data.token;
                        localStorage.setItem('access_token', token);
                        localStorage.setItem('user_id', auth.id);
                        context.commit('retrieveToken', token);
                        context.commit('retrieveAuth', auth.id);
                        context.commit('retrieveAuthCheck', true);
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' +  token;
                        resolve(response);
                    })
                    .catch(error => {
                        localStorage.removeItem('access_token');
                        localStorage.removeItem('user_id');
                        context.commit('destroyToken');
                        context.commit('destroyAuth');
                        context.commit('destroyAuthCheck');
                        reject(error);
                    })
            })
        },
        destroyToken(context) {
            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post(server + 'logout', {
                        'auth': context.getters.auth,
                    })
                        .then(response => {
                            console.log(response);
                            localStorage.removeItem('access_token');
                            localStorage.removeItem('user_id');
                            context.commit('destroyToken');
                            context.commit('destroyAuth');
                            context.commit('destroyAuthCheck');
                            resolve(response);
                        })
                        .catch(error => {
                            console.log(error);
                            reject(error)
                        });
                })
            }
        },
    }
});
